#!/usr/bin/env python

from __future__ import print_function, with_statement
from subprocess import Popen, PIPE, STDOUT
from shlex import split
from threading import Thread
from re import compile, search
from time import time, sleep
from sys import stdout, stderr
from warnings import warn
from SocketServer import TCPServer, StreamRequestHandler, BaseRequestHandler
from time import time
from csv import writer
from tempfile import mktemp
from os import unlink, environ
import SocketServer
from socket import socket, AF_INET, SOCK_STREAM
from subprocess import check_call
import sys
import select

MASTER=False
SYSNAME="Node3"
PING_CMD = "/bin/ping"
AP_IP = "192.168.8.1"
INTERVAL_MS = 200
CMD = "%s -i %0.2f -n %s" % (PING_CMD, INTERVAL_MS / 1000.0, AP_IP)
SEQ_RE = compile("eq=([0-9]+)")
WIRELESS_STATS = "/proc/net/wireless"
DATA_DUMP_PORT = 23412
DATA_DUMP_BLOCK = 1024*128
DATA_DUMP_SIZE = DATA_DUMP_BLOCK*128

def heardCommand():
    i,o,e = select.select([sys.stdin],[],[],0.0001)
    for s in i:
        if s == sys.stdin:
            input = sys.stdin.readline()
            if input:
                return input[0].upper()


class DataDumpTCPHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        ts_in = time()
        byte_count = 0
        if not MASTER:
            stderr.write("\nincoming connection: open\n")
        while True:
            data = self.request.recv(1024)
            byte_count += len(data)
            if not data:
                break
            if byte_count >= DATA_DUMP_SIZE:
                break
            if not MASTER:
                stderr.write("received %d bytes   \r" % byte_count)
        ts_out = time()
        elapsed = ts_out-ts_in
        if not MASTER:
            stderr.write("\nincoming connection: closed. elapsed=%0.2f\n" % elapsed)
        try:
            self.request.send("%0.2f" % elapsed)
        except IOError:
            raise


class DataDumpTCPSender(Thread):
    def __init__(self):
        super(DataDumpTCPSender, self).__init__()
        self.setDaemon(True)
        self.callbacks = []

    def run(self):
        elapsed = None
        try:
            s = socket(AF_INET, SOCK_STREAM)
            s.connect((AP_IP, DATA_DUMP_PORT))
            block = "\0"*DATA_DUMP_BLOCK
            times = DATA_DUMP_SIZE / DATA_DUMP_BLOCK
            assert len(block)*times == DATA_DUMP_SIZE
            for i in range(times):
                s.send(block)
            reply = s.recv(DATA_DUMP_BLOCK)
            elapsed = float(reply.strip())
            s.close()
        except:
            stdout.write("\nERROR executing bandwith test\n")
            raise
        finally:
            for c in self.callbacks:
                try:
                    c(elapsed)
                except:
                    pass


class Status(Thread):
    def __init__(self, datalog_fn):
        super(Status, self).__init__()
        self.daemon = True
        self.expected_seq = 1
        self.last_seq = 1
        self.last_seq_ts = None
        self.prev_seq_ts = None
        self.last_ts = time()
        self.datalog_fn = datalog_fn
        self.datalog = open(self.datalog_fn, "wb")
        self.writer = writer(self.datalog)
        self.dataDumpThread = None
        self.start_ts = None
        stderr.write("\n"
                     "Wi-Fi link quality monitor: started on node %s\n"
                     " - Press T to run backgound bandwidth test\n"
                     " - Press D to download log data (via ZMODEM)\n\n" % SYSNAME)
        self.writer.writerow([
            "time",
            "sample_type",
            "jitter / data_size",
            "link_quality / transfer_time",
            "level / transfer_rate",
            "lost_frames"
        ])

    def run(self):
        self.start_ts = time()
        p = Popen(split(CMD), stdout=PIPE, stderr=STDOUT, bufsize=0)
        for line in iter(p.stdout.readline, ""):
            a = search(SEQ_RE, line)
            if a:
                g = a.groups()
                if g:
                    self.last_seq = int(g[0])
                    self.expected_seq = self.last_seq
                    self.prev_seq_ts = self.last_seq_ts
                    self.last_seq_ts = time()

                    # gap = max(0, seq - self.expected_seq)
                    # self.expected_seq = seq+1
                    # self.missed += gap
                    # ts = time()
                    # if self.last_ts is not None:
                    #     self.jitter_ms = abs(INTERVAL_MS - 1000 * (ts - self.last_ts))
                    # self.last_ts = ts

    def read(self):
        while self.isAlive():
            next_ts = self.last_ts+(INTERVAL_MS/1000.0)
            interval = max(0, next_ts - time())
            sleep(interval)
            self.last_ts = next_ts
            #self.get_quality()
            self.update()
            self.expected_seq += 1

    def get_quality(self):
        with open(WIRELESS_STATS) as fd:
            try:
                i = iter(fd)
                next(i)
                next(i)
                line = next(i)
                toks = line.split()
                if len(toks) != 11:
                    warn("%s file format changed??" % WIRELESS_STATS)
                    return None, None
                quality = int(toks[2].replace(".", ""))
                level = int(toks[3].replace(".", ""))
                return quality, level
            except StopIteration:
                return None, None

    def update(self):
        csv_cells = []
        csv_cells.append("%0.1f" % (time() - self.start_ts)) # 1: ts
        csv_cells.append("sample") # 2: data_type

        jitter_ms = None
        if None not in (self.last_seq_ts, self.prev_seq_ts):
            jitter_ms = abs(INTERVAL_MS - 1000.0 * (self.last_seq_ts - self.prev_seq_ts))
        gap_n = abs(self.expected_seq - self.last_seq)
        gap = "            "
        nl = "\r"
        if gap_n > 2:
            jitter_ms = INTERVAL_MS * gap_n
            gap = "%d LOST FRAMES" % gap_n
            nl = "\n"
        jitter = ("%04.3fmS" % jitter_ms) if jitter_ms else "n/a"
        csv_cells.append(jitter) # 3: jitter

        quality_n, level_n = self.get_quality()
        quality = ("%03d" % quality_n) if quality_n else "n/a"
        csv_cells.append(quality) # 4: quality

        level = ("%03d" % level_n) if level_n else "n/a"
        csv_cells.append(level) # 5: level

        csv_cells.append("%d" % (gap_n if gap_n > 2 else 0)) # 6: lost frames

        self.writer.writerow(csv_cells)
        self.datalog.flush()
        stdout.write("[%s] jitter = %s, link_quality(RSSI) = %s, link_level = %s dBm %s   %s" % (SYSNAME, jitter, quality, level, gap, nl))
        cmd = heardCommand()
        if cmd == "T":
            self.runBandwithTest()
        if cmd == "D":
            self.datalog.flush()
            stdout.write("\nZMODEM download of datalogger data... "
                         "\n(If nothing happens, please start ZMODEM file download on your side!)\n")
            try:
                check_call(["sz", self.datalog_fn])
            except:
                stdout.write("\nZMODEM datalogger data download failed\n")
            finally:
                stdout.write("\nZMODEM datalogger data download terminated\n")
                #self.datalog = open(self.datalog_fn, "wb")
                #self.writer = writer(self.datalog)

    def runBandwithTest(self):
        if self.dataDumpThread is not None:
            stdout.write("\nanother bandwith test is already in progress!\n")
            return
        csv_cells = []
        csv_cells.append("%0.1f" % (time() - self.start_ts)) # 1: ts
        csv_cells.append("bw_test_start")
        csv_cells.append("")
        csv_cells.append("")
        csv_cells.append("")
        csv_cells.append("")
        self.writer.writerow(csv_cells)
        self.datalog.flush()
        stdout.write("\nbandwith test started. sending %d bytes of data...\n" % DATA_DUMP_SIZE)
        self.dataDumpThread = DataDumpTCPSender()
        self.dataDumpThread.callbacks.append(self.onBandwithComplete)
        self.dataDumpThread.start()

    def onBandwithComplete(self, elapsed):
        csv_cells = []
        csv_cells.append("%0.1f" % (time() - self.start_ts)) # 1: ts
        csv_cells.append("bw_test_end")
        csv_cells.append("fail")
        csv_cells.append("")
        csv_cells.append("")
        csv_cells.append("")
        if elapsed is not None:
            mbitps = (float(DATA_DUMP_SIZE)*8/(elapsed*1000000))
            stdout.write("\nbandwith test completed: transferred %d bytes of data "
                         "in %0.2f sec "
                         "(%0.2f Mbit/s)\n" % (
                DATA_DUMP_SIZE, elapsed, mbitps
            ))
            csv_cells[2] = "%d" % DATA_DUMP_SIZE
            csv_cells[3] = "%0.2f" % elapsed
            csv_cells[4] = "%0.2f" % mbitps
            self.dataDumpThread = None
        self.writer.writerow(csv_cells)
        self.datalog.flush()


if __name__ == '__main__':
    datalog_fn = "/tmp/wifi_datalog-%s.csv" % SYSNAME
    data_server = TCPServer(("", DATA_DUMP_PORT), DataDumpTCPHandler)
    data_dump = Thread(target=data_server.serve_forever)
    data_dump.setDaemon(True)
    data_dump.start()
    if not MASTER:
        status = Status(datalog_fn)
        status.start()
        status.read()
    data_dump.join()
