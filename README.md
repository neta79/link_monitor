#  Wi-Fi link realtime monitor

## Layout e setup

Immagine dell'installazione di test (banco prova).


![alt text](img/00_setup.jpg)

Procedendo dall'alto:

* Unità WRTNode, funziona da access-point (pubblica la rete "TestAP"), si trova all'esterno della camera di prova

* Tre unità headless, presenti all'interno della camera di test, le quali funzionano da client wireless (ciascuno si connette in automatico a "TestAP", se in range, indipendentemente dagli altri). In sequenza:
    * Unità RaspberryPi 3, intestata a un cavo ETH. Collegata mediante due UART distinte ai moduli Linkit e WRTNode interni alla camera di test
    * Unità Mediatek Linkit Smart 7688 Duo, headless. La sua tty è intestata sulla UART builtin del Raspberry
    * Unità WRTnode, headless. La sua tty è intestata sulla UART USB installata sul Raspberry

### Connessioni UART tra le unità

Di seguito un close-up della connessione UART tra Raspberry e Linkit:

![alt text](img/01_rpi_linkit_conn.jpg)

Questa connessione funziona in 57600,8n1.

Close-up della connessione UART tra Raspberry e WRTNode:

![alt text](img/02_rpi_wrtnode_conn.jpg)

Questa connessione funziona in 115200,8n1. Da notare che è necessario un piccolo rework per aggiungere
un header 4x100mil. Diversamente la UART del WRTNode non è raggiungibile.

### Connessione Ethernet con l'esterno

Vabbè. E' un RJ45 nell'unica interfaccia ethernet del Raspberry.

![alt text](img/03_rpi_eth.jpg)

## Natura del test

Il test serve a stabilire come (e se) varia la link quality della 
connessione Wi-Fi instaurata tra i moduli interni alla camera di 
test e l'unità "TestAP" che si trova all'esterno, al variare delle
condizioni.

Le condizioni che possono essere variate sono:

* distanza da "TestAP" dalla camera di test
* presenza e fase del gas nella camera di test

I dati prodotti dal test sono:

* Link quality (RSSI), qualora disponibile
* Signal level (espresso in dBm), qualora disponibile
* time jitter
* packet loss

Queste misurazioni sono effettuabili in tempo reale, in parallelo
su tutti e tre i moduli interni alla camera di test.

Durante il test, che una volta avviato ha durata arbitraria, 
ciascuna unità client invia a "TestAP" un pacchetto `ICMP ECHO_REQUEST`
(e TestAP dovrebbe rispondere con `ECHO_REPLY`), con una cadenza di 200mS.

In seguito alla ricezione di un `ECHO_REPLY`, il client disporrà
di un dato RSSI e di signal level aggiornati, che verranno visualizzati.

Qualora questa informazioni non dovessero essere resa disponibili dal
chipset, il test mette a disposizione anche la misura in tempo reale 
del jitter IP e di eventuali pacchetti persi.

Tutti i test sono eseguibili anche quando "TestAP" dovesse risultare
fuori range, in quanto l'operatore controlla le unità raggiungendole
via ethernet e UART (canali indipendenti dal link Wi-Fi).

## Strumenti necessari

* Unità WRTNode etichettata come "TestAP", installata all'esterno della camera di test
* Unità RaspberryPi3 etichettata come "Node1", compresa la sua scheda MicroSD
* Unità Mediatek Linkit Smart 7688 Duo etichettata come "Node2"
* Unità WRTNode etichettata come "Node3"
* 2x cavi connessione UART (3x1), o un arrangiamento equivalente
* Adattatore USB-Seriale 3.3v (nel setup su banco prova ho usato un miserabile CP2102... un qualunque FTDI andrà benissimo)
* Alimentazione per tutta questa roba

* PC con interfaccia ethernet
    * client SSH (Putty se l'O.S è Windows)
    * possibilità di configurare i parametri IP dell'interfaccia ethernet


## Esecuzione del test 

### Connessione e test sull'unità Raspberry

* configurare l'interfaccia ethernet del PC con ip = 192.168.10.11 
    (o qualunque IP sulla 182.168.10/24 ad esclusione di 192.168.10.10)
* con il client ssh, connettersi a `root@192.168.10.10`, password = `open`
    * questo è l'indirizzo statico assegnato all'interfaccia ethernet 
      dell'unità Raspberry
    * dovrebbe succedere questo:

~~~
➜  ~ ssh -l root 192.168.10.10 
root@192.168.10.10's password: 

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Fri Mar 18 16:03:53 2016 from 192.168.10.11
root@raspberrypi:~# 

~~~

* l'unità si connette in automatico a "TestAP" quando questi è in range;
  se lo si vuole verificare, si può testarne la raggiungibilità con 
  `ping -n 192.168.8.1`
* avviare l'utility `link_monitor`. Dopo una fase di warmup iniziale
  (qualche secondo), dovrebbe essere possibile ottenere le misurazioni
  in tempo reale sullo stato del link Wi-Fi:

~~~
root@raspberrypi:~# link_monitor 
[RasPi3] jitter = 0.637mS, link_quality(RSSI) = 070, link_level = -19 dBm
~~~

* `link_monitor` è interrompibile con `Ctrl+C` in qualunque momento
* Mentre `link_monitor` funziona, è comunque possibile aprire una nuova
  connessione SSH verso `root@192.168.10.10` per procedere con le
  parti del test descritte di seguito

  
### Connessione e test sull'unità Linkit Smart 7688

* Una volta connessi alla shell dell'unità Raspberry (vedere paragrafo 
precedente), invocare il comando `tty_linkitsmart` per aprire una connessione
con l'unità Mediatek Linkit Smart 7688
    * Compare una schermata nera. Premere return per rinfrescare la shell

* E' ora possibile avviare `link_monitor`, in maniera analoga a quanto fatto sull'unità Raspberry

Esempio:

~~~
root@raspberrypi:~# tty_linkitsmart 
root@mylinkit:/# link_monitor 
[LinkIt Smart] jitter = 0.244mS, link_quality(RSSI) = 010, link_level = -256 dBm
~~~

* `link_monitor` è interrompibile con `Ctrl+C`, questo ci riporta alla shell
  di controllo dell'unità Linkit Smart. Per tornare alla shell dell'unità Raspberry,
  Premere `Ctrl+A` seguito da `K`, e rispondere `Y` per conferma.

  
### Connessione e test sull'unità WRTNode

Tutto quanto detto al paragrafo precedente vale anche per l'unità WRTNode,
ma bisogna usare il comando `tty_wrtnode` per connettervisi.


## Monitoraggio del packet-loss

Se si verifica del packet-loss in modo consistente durante il test, l'unità "TestAP" è da considerarsi
fuori range. L'utility `link_monitor` logga esplicitamente il numero di pacchetti persi,
lasciando uno storico a schermo. In questo modo l'informazione non va persa rapidamente.

~~~
...
[WRTNode] jitter = 2000.000mS, link_quality(RSSI) = n/a, link_level = n/a dBm 10 LOST FRAMES   
[WRTNode] jitter = 2200.000mS, link_quality(RSSI) = n/a, link_level = n/a dBm 11 LOST FRAMES   
[WRTNode] jitter = 2400.000mS, link_quality(RSSI) = n/a, link_level = n/a dBm 12 LOST FRAMES   
[WRTNode] jitter = 2600.000mS, link_quality(RSSI) = n/a, link_level = n/a dBm 13 LOST FRAMES   
...
~~~

Da notare comunque che in caso di packet-loss il jitter aumenta enormemente, e questo
da solo deve valere come prova che il link si è degratado oltre i livelli accettabili.


## Funzionalità di data logger

`link_monitor` include una funzione di data logging. Per scaricare
il dump completo dei dati relativi al run corrente, premere il tasto 
`D` seguito da `Invio`. (Nota: Perché questa funzionalità sia
disponibile, è necessario connettersi a Nodo1 usando un client SSH
capace di gestire trasferimenti ZMODEM in linea.

Su windows il funzionamento è testato con [Xshell](https://www.netsarang.com/products/xsh_overview.html).

Il comune `Putty` non include questa funzione. (Forse uno dei numerosi 
suoi fork sì, ma per semplicità è consigliata l'installazione di Xshell,
che include una licenza free-for-noncommercial). 


## Bandwidth test

Durante il run di `link_monitor` è possibile effettuare un breve test 
per la stima dell'ampiezza di banda, premendo il tasto `T` seguito da
`Invio`.

Il test viene condotto in background, e consiste nel trasferimeno 
cronometrato di un payload a grandezza nota.
Al termine del test verrà visualizzata una riga recante una stima
sull'ampiezza di banda misurata.

Il risultato del test viene inoltre includo nel datalog scaricabile.
